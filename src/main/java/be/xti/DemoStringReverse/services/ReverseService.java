package be.xti.DemoStringReverse.services;

public interface ReverseService {
	public String requestReverse(String stringToReverse);
	
	public String reverse(String stringToReverse);
}
