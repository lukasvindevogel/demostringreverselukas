package be.xti.DemoStringReverse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ReverseServiceImpl implements ReverseService {
	@Value("${baseurl.stefan}")
	private String BASE_URL;

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public String requestReverse(String stringToReverse) {
		try {
			return restTemplate.getForObject(BASE_URL + "/api/reverse?stringToReverse=" + stringToReverse, String.class);
		} catch (Exception e) {
			return "SOMETHING WENT WRONG:\n" + e.getMessage();
		}
	}

	@Override
	public String reverse(String stringToReverse) {
		return new StringBuilder(stringToReverse).reverse().toString();
	}

}
