package be.xti.DemoStringReverse.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import be.xti.DemoStringReverse.services.ReverseService;

@RestController
public class stringReverseController {
	
	@Autowired
	private ReverseService reverseService;
	
	@GetMapping("/reverse")
	public String requestReverse(String stringToReverse) {
		return reverseService.requestReverse(stringToReverse);
	}
	
	@GetMapping("api/reverse")
	public String reverse(String stringToReverse) {
		return reverseService.reverse(stringToReverse);
	}
}